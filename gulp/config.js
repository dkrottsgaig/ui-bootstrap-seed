/**
 * Gulp configuration file
 */
var pkg = require('../package.json');
var config = {};

/**
 * Define build src and dist paths
 */
config.paths = {
  src: {
    root: './src',
    common: './src/common',
    modules: './src/modules'
  },

  dist: {
    root: './dist',
    css: './dist/css',
    templates: './dist/templates',
    js: './dist/js',
    vendor: './dist/js/vendor'
  }
};

/**
 * Configure webserver
 */
config.webserver = {
  port: 9000
};

/**
 * Define replacement patterns
 */
config.replacements = [
  {
    pattern: /\$\{common_src\}/g,
    replacement: config.paths.src.common
  },
  {
    pattern: /\$\{modules_src\}/g,
    replacement: config.paths.src.modules
  },
  {
    pattern: /\$\{version\}/g,
    replacement: pkg.version
  },
  {
    pattern: /\$\{uiBootstrapCDN\}/g,
    replacement: pkg.uiBootstrapCDN
  },
  {
    pattern: /\$\{uiBootstrapVersion\}/g,
    replacement: pkg.uiBootstrapVersion
  },
  {
    pattern: /<!-- inject:uiBootstrapCss -->/g,
    replacement: '<link rel="stylesheet" href="' + pkg.uiBootstrapCDN + '/'
      + pkg.uiBootstrapVersion + '/css/gaig-bootstrap.css">'
  },
  {
    pattern: /<!-- inject:uiBootstrapJs -->/g,
    replacement: '<script src="' + pkg.uiBootstrapCDN + '/' + pkg.uiBootstrapVersion
      + '/js/gaig-ui.js"></script>'
  }
];

/**
 * Karma config
 */
config.karma = {
  logLevel: 'LOG_DEBUG' // LOG_DISABLE, LOG_ERROR, LOG_WARN, LOG_INFO, LOG_DEBUG
};

module.exports = config;