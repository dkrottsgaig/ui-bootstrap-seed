<div class="navbar navbar-static-top">
  <div class="navbar-inner">
    <div class="container">
      <div class="pull-left">
        <ul class="nav">

          <li>
            <a href="#/example">
              Example Page
            </a>
          </li>

          <li class="dropdown" gaig-dropdown>
            <a href="#" class="dropdown-toggle">
              Menu Item
              <b class="caret"></b>
            </a>

            <ul class="dropdown-menu dropdown-menu-alt">
              <li>
                <a href="#/enroll">Dropdown Link</a>
              </li>
              <li>
                <a href="#/search">Dropdown Link</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="pull-right">
        <ul class="nav">
          <li class="dropdown" gaig-dropdown>
            <a href="#" class="dropdown-toggle">
              <span class="gaig-icon gaig-icon-user"></span> Username
              <b class="caret"></b>
            </a>

            <ul class="dropdown-menu dropdown-menu-alt">
              <li>
                <a href="#/logout">Logout</a>
              </li>
            </ul>
          </li>
        </ul>

        <a href="#/" class="brand brand-extended">
          Application Name
          <span class="brand-line-two">Business Unit</span>
        </a>
      </div>
    </div>
  </div>
</div>

<ng-include src="'templates/navhelper.html'"></ng-include>