'use strict';

module.exports = angular.module('gaigApp.header', [])
  .controller('NavhelperCtrl', require('./controllers/navhelper.controller.js'));