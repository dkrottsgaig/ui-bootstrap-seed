'use strict';

/**
 * @ngInject
 */
module.exports = function NavhelperCtrl($scope, metaService) {
  var vm = this;

  (function init() {
    vm.model = {
      pageTitle: null,
      pageIcon: null
    };

    metaService.registerObserver(updateNavhelper, $scope);
  })();

  /* ===============================================================================================
   Members
   ============================================================================================== */


  /* ===============================================================================================
   Private
   ============================================================================================== */
  function updateNavhelper() {
    vm.model.pageIcon = metaService.model.pageIcon;
    vm.model.pageTitle = metaService.model.pageTitle;
  }

};