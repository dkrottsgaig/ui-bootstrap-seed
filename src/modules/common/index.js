'use strict';

module.exports = angular.module('gaigApp.common', [])

  .controller('GlobalCtrl', require('./controllers/global.controller.js'))

  .service('metaService', require('./services/meta.service.js'));
