'use strict';

/**
 * @ngInject
 */
module.exports = function metaService($rootScope, $route, appSettings) {
  var meta = this;
  var observers = [];

  /* ===============================================================================================
   Init
   ============================================================================================== */
  (function init() {

    meta.model = {
      pageIcon: $route.current.pageIcon,
      pageTitle: $route.current.pageTitle
    };

    $rootScope.$on('$routeChangeStart', handleRouteChangeStart);
    $rootScope.$on('$routeChangeSuccess', handleRouteChangeSuccess);
  })();

  /* ===============================================================================================
   Member functions
   ============================================================================================== */
  meta.registerObserver = function (fn, scope) {
    var observerExists = false;
    var currentIndex = 0;
    var observersLength = observers.length;

    // execute observer function once
    fn();

    for (currentIndex; currentIndex < observersLength; currentIndex++) {
      if (observers[currentIndex] === fn) {
        observerExists = true;
        break;
      }
    }

    if (!observerExists) {
      observers.push(fn);
    }

    if (angular.isDefined(scope)) {
      scope.$on('$destroy', function () {
        meta.deregisterObserver(fn);
      });
    }
  };

  meta.deregisterObserver = function (fn) {
    var ret = false;
    var currentIndex = 0;
    var observersLength = observers.length;

    for (currentIndex; currentIndex < observersLength; currentIndex++) {
      if (observers[currentIndex] === fn) {
        observers.splice(currentIndex, 1);
        ret = true;
        break;
      }
    }

    return ret;
  };

  meta.notifyObservers = function () {
    angular.forEach(observers, function (observer) {
      observer();
    });
  };

  /* ===============================================================================================
   Private functions
   ============================================================================================== */
  function handleRouteChangeStart() {
    meta.model.pageIcon = null;
    meta.model.pageTitle = appSettings.loadingString;
    meta.notifyObservers();
  }

  function handleRouteChangeSuccess() {
    meta.model.pageIcon = $route.current.pageIcon;
    meta.model.pageTitle = $route.current.pageTitle;
    meta.notifyObservers();
  }
};